package com.spark.match.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;
import com.spark.match.repo.MatchRepository;
import com.spark.match.test.util.DummyObjectFactory;

public class MatchServiceTest {
	
	@InjectMocks
	MatchServiceImpl matchService;
	
	@Mock
	MatchRepository matchRepository;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetMatchById() throws IllegalArgumentException, IllegalAccessException {
		Match dummyMatch = (Match) DummyObjectFactory.populateDummyObject(new Match());
		Long dummyId = 101L;
		dummyMatch.setId(dummyId);
		
		when(matchRepository.findById(dummyId)).thenReturn(Optional.ofNullable(dummyMatch));		
		assertEquals(dummyId, matchService.getMatchById(dummyId).get().getId());
	}
	
	@Test
	public void testGetMatches() throws IllegalArgumentException, IllegalAccessException {
		List<Match> dummyMatchList = new ArrayList<>();
		dummyMatchList.add((Match) DummyObjectFactory.populateDummyObject(new Match()));
		dummyMatchList.add((Match) DummyObjectFactory.populateDummyObject(new Match()));
		
		when(matchRepository.getMatches(Mockito.any(FilterCriteria.class))).thenReturn(dummyMatchList);
		assertEquals(2, matchService.getMatches(new FilterCriteria()).size());
	}

}
