package com.spark.match.repo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;
import com.spark.match.util.CommonUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MatchRepositoryImpl implements MatchRepositoryCustom {

	private final String LAT_LON_DIST_CLAUSE = "acos(sin(radians(:paramLat)) * sin(radians(lat)) "
			+ "+ cos(radians(:paramLat)) * cos(radians(lat)) * cos(radians(lon) - radians(:paramLon))) * 6371 <= :dist";

	@PersistenceContext
	private EntityManager entityManager;

	private Map<String, Object> paramMap = new LinkedHashMap<>();

	@Override
	public List<Match> getMatches(FilterCriteria filterCriteria) {

		log.info("Start - filer matches;  filer criteria: {}", filterCriteria);

		Query query = entityManager.createQuery(buildQueryString(filterCriteria));

		for (Map.Entry<String, Object> paramEntry : paramMap.entrySet()) {
			query.setParameter(paramEntry.getKey(), paramEntry.getValue());
			log.info("Search Parameters - Name: " + paramEntry.getKey() + " Value: " + paramEntry.getValue());
		}
		paramMap.clear();
		List<Match> matches = query.getResultList();
		log.info("End - filer matches; result size: {}", matches.size());
		return matches;
	}

	private String buildQueryString(FilterCriteria filterCriteria) {

		StringBuilder queryStr = new StringBuilder("select m from Match m");

		int noOfClause = 0;

		// exclude current user
		if (filterCriteria.getCurrentId() != null && filterCriteria.getCurrentId() != 0) {
			noOfClause++;
			if (noOfClause == 1) {
				queryStr.append(" where ");
			} else if (noOfClause > 1) {
				queryStr.append(" and ");
			}
			queryStr.append("m.id <> :currentId");
			paramMap.put("currentId", filterCriteria.getCurrentId());
		}

		// select matches only with photo
		if (filterCriteria.isHasPhoto() == true) {
			noOfClause++;
			if (noOfClause == 1) {
				queryStr.append(" where ");
			} else if (noOfClause > 1) {
				queryStr.append(" and ");
			}
			queryStr.append("m.mainPhoto is not null");
		}

		// select matches who are fav
		if (filterCriteria.isFavourite() == true) {
			noOfClause++;
			if (noOfClause == 1) {
				queryStr.append(" where ");
			} else if (noOfClause > 1) {
				queryStr.append(" and ");
			}
			queryStr.append("m.favourite = true");
		}

		// select matches who are inContact
		if (filterCriteria.isInContact() == true) {
			noOfClause++;
			if (noOfClause == 1) {
				queryStr.append(" where ");
			} else if (noOfClause > 1) {
				queryStr.append(" and ");
			}
			queryStr.append("m.contactsExchanged > 0");
		}

		// select matches with comp score
		if (filterCriteria.getCompatibilityScore() != null && filterCriteria.getCompatibilityScore().getMax() <= 99
				&& filterCriteria.getCompatibilityScore().getMin() >= 1) {
			noOfClause++;
			if (noOfClause == 1) {
				queryStr.append(" where ");
			} else if (noOfClause > 1) {
				queryStr.append(" and ");
			}
			queryStr.append("m.compatibilityScore between :minCs and :maxCs");
			paramMap.put("minCs", CommonUtils.convertPrcntgToFrctn(filterCriteria.getCompatibilityScore().getMin()));
			paramMap.put("maxCs", CommonUtils.convertPrcntgToFrctn(filterCriteria.getCompatibilityScore().getMax()));
		}

		// select matches with age range
		if (filterCriteria.getAge() != null && filterCriteria.getAge().getMax() <= 95
				&& filterCriteria.getAge().getMin() >= 18) {
			noOfClause++;
			if (noOfClause == 1) {
				queryStr.append(" where ");
			} else if (noOfClause > 1) {
				queryStr.append(" and ");
			}
			queryStr.append("m.age between :minAge and :maxAge");
			paramMap.put("minAge", filterCriteria.getAge().getMin());
			paramMap.put("maxAge", filterCriteria.getAge().getMax());
		}

		// select matches with height range
		if (filterCriteria.getHeight() != null && filterCriteria.getHeight().getMax() <= 210
				&& filterCriteria.getHeight().getMin() >= 135) {
			noOfClause++;
			if (noOfClause == 1) {
				queryStr.append(" where ");
			} else if (noOfClause > 1) {
				queryStr.append(" and ");
			}
			queryStr.append("m.heightInCm between :minHeight and :maxHeight");
			paramMap.put("minHeight", filterCriteria.getHeight().getMin());
			paramMap.put("maxHeight", filterCriteria.getHeight().getMax());
		}

		// select matches withing distance in km
		if (filterCriteria.getLocDist() != null && filterCriteria.getLocDist().getLat() != null
				&& filterCriteria.getLocDist().getLon() != null
				&& filterCriteria.getLocDist().getDistanceInKm() != null) {
			noOfClause++;
			if (noOfClause == 1) {
				queryStr.append(" where ");
			} else if (noOfClause > 1) {
				queryStr.append(" and ");
			}
			queryStr.append(LAT_LON_DIST_CLAUSE);
			paramMap.put("paramLat", filterCriteria.getLocDist().getLat());
			paramMap.put("paramLon", filterCriteria.getLocDist().getLon());
			paramMap.put("dist", filterCriteria.getLocDist().getDistanceInKm());
		}

		return queryStr.toString();
	}

}
